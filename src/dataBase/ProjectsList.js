import Agora from '../assets/static/Agora2.png'

const projects = [
  {
    title: 'Commerce - Next',
    img: 'https://ibb.co/Sf8Rm5z',
    description:
      'Aplicación Web de E-commerce realizada con un equipo de 6 personas, utilizando el stack completo de Javascript y metodologías ágiles + Trello',
    animation: 'flip-left',
    technologies: [
      'React',
      'Redux',
      'Node',
      'Express',
      'PostgreSQL',
      'Sequelize',
      'Bootstrap',
      'Passport',
      'Formik',
      'SCRUM'
    ],
    deploy: 'https://app-next-ruby.vercel.app/',
    repositorio: 'https://gitlab.com/juandavid.estevesgirata/app-next'
  },
  {
    img: Agora,
    title: 'Agora',
    description:
      'Aplicación web de formato educativo realizada con un equipo de 11 personas, utilizando React + Express + MongoDB y metodologías ágiles + Jira',
    animation: 'flip-left',
    technologies: [
      'HTML',
      'JavaScript',
      'CSS',
      'Animate.css',
      'Style JSX',
      'React',
      'React Hook Form',
      'Redux',
      'MongoDB',
      'Node',
      'Express',
      'JsonWebToken',
      'Mongoose',
      'Formik',
      'SCRUM'
    ],
    deploy: false,
    repositorio: 'https://github.com/JuanEstevesGirata/agora-app'
  },
  {
    img: 'https://ibb.co/WKCxCdH',
    title: 'Portafolio',
    description:
      'Aplicación Web realizada desde cero con React, para mostrar mis trabajos personales como developer, incorporando animaciones, BEM y SASS.',
    animation: 'flip-left',
    technologies: [
      'HTML',
      'JavaScript',
      'CSS',
      'Style JSX',
      'React',
      'SASS',
      'React Hook Form',
      'Animate.css',
      'React-Scroll'
    ],
    deploy: 'https://portfolio-pdlyromwv-juandavidestevesgirata.vercel.app/',
    repositorio: 'https://gitlab.com/juandavid.estevesgirata/portfolio'
  }
]

export default projects
