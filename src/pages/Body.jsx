import Contact from '../components/Contact'
import Home from '../components/Home'
import Projects from '../components/Projects/Projects'
import Skills from '../components/Skills'

const Body = () => {
  return (
    <>
      <Home id='home' />
      <Skills id='skills' />
      <Projects id='projects' />
      <Contact id='contact' />
    </>
  )
}

export default Body
