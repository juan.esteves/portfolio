import React, { useState } from 'react'
import Footer from './components/Footer/Footer'
import Body from './pages/Body'
import Header from './components/Header'
import ThemeContext, { darkMode } from './components/ThemeContext'
import Info from './components/Footer/Info'

function App () {
  const [theme, setTheme] = useState(darkMode)

  const toggleTheme = () => {
    theme
      ? localStorage.setItem('dark-mode', 'false')
      : localStorage.setItem('dark-mode', 'true')
    setTheme(!theme)
  }

  return (
    <ThemeContext.Provider value={theme}>
      <div
        style={
          theme
            ? { backgroundColor: '#f0f0f0' }
            : { backgroundColor: '#242526' }
        }
      >
        <Header toggleTheme={toggleTheme} />
        <Body />
        <Info />
        <Footer />
      </div>
    </ThemeContext.Provider>
  )
}

export default App
