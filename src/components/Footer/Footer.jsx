import React from 'react'
import ThemeContext from '../ThemeContext'

const Footer = () => {
  const darkMode = React.useContext(ThemeContext)

  return (
    <>
      <footer>
        <div className='footer-container'>
          <div className='footer-content-container'>
            <h3 className='website-logo'>About Me</h3>
            <span className='footer-info'>
              Magister en Ingeniería y Desarrollador Full-Stack. Con
              competencias para la creación y consumo de APIS, y generación de
              interfaces de usuarios. Desde las cuales construyo soluciones
              tecnológicas óptimas, seguras y escalables.{' '}
            </span>

            <div className='footer-content-links'>
              <div className='social-container'>
                <a href='https://github.com/JuanEstevesGirata'>
                  <i className='fab fa-github fa-2x' aria-hidden='true'></i>
                </a>
                <a href='https://gitlab.com/juandavid.estevesgirata'>
                  <i className='fab fa-gitlab fa-2x' aria-hidden='true'></i>
                </a>
                <a href='https://www.linkedin.com/in/juan-david-esteves-girat%C3%A1-862394219/'>
                  <i
                    className='fab fa-linkedin-in fa-2x'
                    aria-hidden='true'
                  ></i>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className='copyright-container'>
          <span className='copyright'>
            Copyright 2021, itskrey.com. All rights reserved.
          </span>
        </div>
      </footer>
      <style jsx>
        {`
          footer {
            font-family: 'Caveat Brush', cursive;
            font-size: 20px;
            position: fixed;
            z-index: -2;
            bottom: 0;
            left: 0;
            height: fit-content;
            width: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            background-color: ${darkMode ? '#ffffff' : '#161B22'};
            color: ${darkMode ? '#161B22' : '#ffffff'};
            padding-top: 180px;
            text-align: center;
          }

          .copyright-container {
            width: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            border-top: 1px solid rgba(255, 255, 255, 0.3);
          }

          .copyright {
            font-size: 12px;
            opacity: 0.7;
            font-weight: 400;
            padding: 10px 0;
          }

          .footer-container {
            height: fit-content;
            width: 100%;
            padding: 3rem 6rem;
            box-sizing: border-box;
            display: flex;
            justify-content: space-between;
            text-align: center;
          }

          .footer-content-container {
            display: flex;
            justify-content: space-around;
            flex-direction: column;
          }

          .footer-content-links {
            width: 100%;
            display: flex;
            wrap: wrap;
            justify-content: space-between;
            color: ${darkMode ? '#161B22' : '#ffffff'};
            flex-direction: column;
          }

          footer .website-logo {
            margin-bottom: 1.2rem;
            font-size: calc(1vw + 20px);
          }

          .footer-info,
          .menu-item-footer {
            margin: 0.2rem 0;
            opacity: 0.7;
            color: ${darkMode ? '#161B22' : '#ffffff'};
            text-decoration: none;
            transition: 0.5s;
          }

          .menu-item-footer:hover {
            opacity: 1;
          }

          .menu-title {
            font-size: var(--medium-text-font);
            font-weight: 400;
            text-transform: uppercase;
            margin-bottom: 1.2rem;
          }

          .social-container {
            display: flex;
            justify-content: center;
            padding: 25px;
            height: 30px;
          }

          .social-container a {
            padding: 20px;
            color:${darkMode ? '#161B22' : '#ffffff'};

          .social-link {
            height: 100%;
            width: 30px;
            background-image: url(FacebookBlanco.png);
            background-size: 70%;
            background-position: center;
            margin-right: 1rem;
            background-repeat: no-repeat;
          }

          .social-link:hover {
            opacity: 0.7;
          }

          footer .social-link:nth-of-type(2) {
            background-image: url(TwitterBlanco.png);
          }

          footer .social-link:nth-of-type(3) {
            /*Nth of type inside that container*/
            background-image: url(LinkedinBlanco.png);
          }

          /*MEDIA QUERY*/
          @media (max-width: 768px) {
            .footer-container {
              padding: 2rem;
              flex-direction: column;
            }

            .footer-content-container {
              width: 100%;
              padding: 2rem 0;
              border-bottom: 1px solid #2a2a2a;
              justify-content: center;
              align-items: center;
            }

            .footer-content-container:nth-of-type(3) {
              border-bottom: none;
              padding-bottom: 0;
            }

            .footer-menus {
              width: 100%;
              display: flex;
              justify-content: space-between;
              align-items: flex-start;
              border-bottom: 1px solid #2a2a2a;
            }
          }
        `}
      </style>
    </>
  )
}

export default Footer
