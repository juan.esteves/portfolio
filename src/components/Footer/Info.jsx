import React from 'react'
import ThemeContext from '../ThemeContext'

const Info = () => {
  const darkMode = React.useContext(ThemeContext)
  return (
    <>
      <div className='info_container'>
        <div className='info'>
          <h1>Let's work together and create something unique</h1>
        </div>
      </div>
      <style jsx>
        {`
          .info_container {
            font-family: 'Caveat Brush', cursive;
            width: 100%;
            height: 300px;
            background-color: ${darkMode ? '#161B22' : '#ffffff'};
            color: ${darkMode ? '#ffffff' : '#161B22'};
            margin-bottom: 100vh;
          }

          .info {
            font-family: 'Caveat Brush', cursive;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
          }

          @media only screen and (max-width: 960px) {
            .info-container {
              height: 200px;
              width: 100%;
            }

            .info > h1 {
              text-align: center;
              font-size: 25px;
            }
          }
        `}
      </style>
    </>
  )
}

export default Info
