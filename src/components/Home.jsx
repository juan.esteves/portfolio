import React from 'react'
import style from '../assets/styles/components/home.module.scss'
import ThemeContext from './ThemeContext'

const Home = ({ id, title }) => {
  const darkMode = React.useContext(ThemeContext)

  return (
    <div
      className={
        darkMode
          ? [style.home_container, style.dark].join(' ')
          : style.home_container
      }
      id={id}
    >
      <div className={style.about}>
        <h1 className={style.name}>Juan David Esteves Girata</h1>
        <h1 className={style.responsive_name}>Jd</h1>

        <p>
          Full Stack Developer | Front-end | Back-end<span>&#160;</span>
        </p>
        <p>
          HTML | CSS | Python | JavaScript | React | NodeJS | Django | MongoDB |
          SQL <span>&#160;</span>
        </p>
      </div>
      <figure className={style.about_img} width='20%' height='20%'>
        <img alt='' />
        <div class={style.capa}>
          <h3>About Me</h3>
          <p>
            Magister en Ingeniería y Desarrollador Full-Stack. Construyo
            soluciones tecnológicas óptimas, seguras y escalables.{' '}
          </p>
        </div>
      </figure>
      <style jsx>
        {`
          span {
            background-color: ${darkMode ? '#ffffff' : '#161B22'};
          }
        `}
      </style>
    </div>
  )
}

export default Home
