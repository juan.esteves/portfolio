import React from 'react'
import style from '../../assets/styles/components/projects.module.scss'
import projects from '../../dataBase/ProjectsList'
import Project from './Project'
import ThemeContext from '../ThemeContext'
import AgoraImg from '../../assets/static/Agora.png'
import PortafolioImg from '../../assets/static/Portafolio.png'
import NextImg from '../../assets/static/app-next.png'


const Projects = ({ id, title }) => {
  const darkMode = React.useContext(ThemeContext)
  let images = [NextImg, AgoraImg, PortafolioImg]

  return (
    <div
      id={id}
      className={
        darkMode ? [style.projects, style.dark].join(' ') : style.projects
      }
    >
      <h1>Projects</h1>
      <div className={style.projectsContainer}>
        {projects.map((project, i) => (
          <Project project={project} img={images[i]} key={i} />
        ))}
      </div>
    </div>
  )
}

export default Projects
