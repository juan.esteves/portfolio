import React, { useEffect } from 'react'
import style from '../../assets/styles/components/projects.module.scss'
import Aos from 'aos' 
import 'aos/dist/aos.css'
import ThemeContext from '../ThemeContext'

const Project = ({ project, img }) => {
  const darkMode = React.useContext(ThemeContext)
  
  useEffect(() => {
    Aos.init({ duration: 2000 })
  }, [])

  return (
    <div
      className={darkMode ? [style.card, style.dark].join(' ') : style.card}
      data-aos='zoom-in'
    >
      <div>
        <h3 className={style.title}>{project.title}</h3>
        <img className={style.img} src={img} alt='project img' />
          
      </div>
      <div>
        <p className={style.description}>{project.description}</p>
      </div>
      <div>
        <>
          <div className={style.linkContainer}>
            {project.deploy ? (
              <a
                className={style.link}
                href={project.deploy}
                target='_blank'
                rel='noreferrer'
              >
                Deploy
              </a>
            ) : (
              <div className={style.noLink}>Deploy</div>
            )}
            {project.repositorio ? (
              <a
                className={style.link}
                href={project.repositorio}
                rel='noreferrer'
                target='_blank'
              >
                Repository
              </a>
            ) : (
              <div className={style.noLink}>Repository</div>
            )}
          </div>
        </>
        <>
          <div className={style.container}>
            <h4 className={style.techStack}>Tech Stack</h4>
            <div className={style.techContainer}>
              {project.technologies.map((tech, e) => (
                <p key={e} className={style.technologies}>
                  {tech}
                </p>
              ))}
            </div>
          </div>
        </>
      </div>
    </div>
  )
}

export default Project
